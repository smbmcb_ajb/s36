const Task = require('../models/Task.js');

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})

	return newTask.save().then((savedTask, error) => {
		if (error) {
			console.log(error)
			return error
		}

		return savedTask
	})
}

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result
	})
}

module.exports.updateTask = (task_id, new_data) =>{
	return Task.findById(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}

		result.name = new_data.name

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return updatedTask
		})
	})
}

module.exports.updateTaskStatus = (task_id, task_status) => {
	return Task.findById(task_id).then((result, error) => {

	
		if(error){
			console.log(error)
			return error
		}

		result.Status = task_status

		return result.save().then((udpatedTaskStatus, error) => {
			if(error){
				console.log(error)
				return error
			}

			return udpatedTaskStatus
		})
	})
}


module.exports.getSpecificTask = (task_id) => {
	return Task.findById(task_id).then((result, error) => {

		if(error){
			console.log(error)
			return error
		}

		return result

	})
}

module.exports.deleteTask = (task_id) => {
	return Task.deleteOne({
		_id: task_id
	}).then((result, error) => {
		return message = `Task ID: ${task_id} Successfully deleted`
	})

	
}