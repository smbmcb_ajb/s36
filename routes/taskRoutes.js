const express = require('express');
const router = express.Router();
const TaskController = require('../controllers/TaskController')
router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})

// Update a task
router.patch('/:id/update', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

// Update a task status
router.patch('/:id/:status', (request, response) => {
	TaskController.updateTaskStatus(request.params.id, request.params.status).then((result) => {
		response.send(result)
	})
})

// Update a task status
router.get('/:id', (request, response) => {
	TaskController.getSpecificTask(request.params.id).then((result) => {
		response.send(result)

	})
})


// Delete task

router.delete('/:id/delete', (request, response) => {
	TaskController.deleteTask(request.params.id).then((result) => {
		response.send(result)
	})
})


module.exports = router